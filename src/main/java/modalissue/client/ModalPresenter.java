package modalissue.client;

import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.googlecode.slotted.client.SlottedActivity;


public class ModalPresenter extends SlottedActivity {

    
	public ModalPresenter() {

	}

	@Override
	public void start(AcceptsOneWidget panel) {
    	panel.setWidget(new ModalView());
	}


}

