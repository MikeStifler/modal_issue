package modalissue.client;


import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.googlecode.slotted.client.AutoHistoryMapper;
import com.googlecode.slotted.client.HistoryMapper;
import com.googlecode.slotted.client.SlottedController;
import com.googlecode.slotted.client.SlottedEventBus;

public class EntryPoint implements com.google.gwt.core.client.EntryPoint {


	public void onModuleLoad()		{
		
// This pattern for the GWT entry point uses the slotted framework http://code.google.com/p/slotted/
		HistoryMapper historyMapper = GWT.create(AutoHistoryMapper.class);
		SlottedEventBus eventBus = new SlottedEventBus();
		SlottedController slottedController = new SlottedController(historyMapper, eventBus);
		slottedController.setDefaultPlace(new ModalPlace());
		SimplePanel rootDisplay = new SimplePanel();
		RootPanel.get().add(rootDisplay);
		slottedController.setDisplay(rootDisplay); 
		
	}


}